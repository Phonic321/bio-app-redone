package edu.udmercy.bioapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class MainActivity2 : AppCompatActivity() {


    private lateinit var final_message: String

    private lateinit var bio: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)


        bio = findViewById(R.id.bio)

        final_message = intent.getStringExtra("bio").toString()

        bio.setText(final_message)

    }
}