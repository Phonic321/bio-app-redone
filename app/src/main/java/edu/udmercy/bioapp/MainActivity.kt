package edu.udmercy.bioapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class MainActivity : AppCompatActivity() {

    private lateinit var createButton: Button
    private lateinit var fname: EditText
    private lateinit var lname: EditText
    private lateinit var schoolPlain: EditText
    private lateinit var yearPlain: EditText
    private lateinit var bs: RadioButton
    private lateinit var ms: RadioButton
    private lateinit var phd: RadioButton
    private lateinit var rg: RadioGroup
    private lateinit var degree: String
    private lateinit var finalMessage: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createButton = findViewById(R.id.createButton)
        fname = findViewById(R.id.firstNamePlain)
        lname = findViewById(R.id.lastNamePlain)
        schoolPlain = findViewById(R.id.schoolPlain)
        yearPlain = findViewById(R.id.yearPlain)
        bs = findViewById(R.id.bsButton)
        ms = findViewById(R.id.msButton)
        phd = findViewById(R.id.phdButton)


        createButton.setOnClickListener {
            degree = ""

            if (bs.isChecked)
            {
                degree = "Bachelor's degree."
            }
            if (ms.isChecked)
            {
                degree = "Master's degree."
            }
            if (phd.isChecked)
            {
               degree = "P.H.D."
            }

            //Toast.makeToast(this, fname.getText().toString() + " " + lname.getText().toString() + " is going to graduate from " + schoolPlain.getText().toString() + " in " + yearPlain.getText().toString() + " with a " + degree,Toast.LENGTH_LONG)
                //.show()

            finalMessage = fname.getText().toString() + " " + lname.getText().toString() + " is going to graduate from " + schoolPlain.getText().toString() + " in " + yearPlain.getText().toString() + " with a " + degree

            val intent = Intent(this, MainActivity2::class.java)
            intent.putExtra("bio", finalMessage)
            startActivity(intent)
        }
    }

}